###LovePanda博客
	2016年1月17日，本周末更新已完成。
	此次更新主要是更新博客美图的列表页面的自适应能力，
	首页推荐图片在手机浏览器下的自适应能力。展示的content减少为100字，增加了全局的事务。
	
##项目介绍：
	lovepanda博客是博主自己在工作的空闲之余抱着学习的心态自己开发的一个博客系统，前端UI为Amaze UI，
	后台为目前最火的Jfinal，使用Ehcache缓存，弹出层为Layer，富文本编辑器为kindeditor和Ueditor双编辑器，数据库MySQL。
	博主的线上部署地址为[www.liuyunfei.cn](http://www.liuyunfei.cn)感兴趣的朋友可以去看看。
	
#申明：
	此博客为博主自己兴趣而开发的，博主在编写的时候工作不到半年，陆陆续续的编写到工作差不多一年（也即是现在）才编写完成。
	其中借鉴了一些项目的样式和部分代码，ui方面借鉴了[宠物秀](http://www.petshow.cc/)的样式，
	资源监控借鉴了[JfinalUIB](http://git.oschina.net/dongcb678/JfinalUIB)的一个util，当然还有其他零零碎碎的东西。
	
#展示：
前台
![前台界面](http://www.liuyunfei.cn/attached/image/20160117/20160117181437_392.png)

后台	
![后台界面](http://www.liuyunfei.cn/attached/image/20160117/20160117174642_976.png)

具体的部署文档和初始化sql在项目的doc文件夹下，在此就不累述了。
后台初始化账号和密码均为：admin

#最后谢谢大家的支持！☆⌒(*＾-゜)v THX!!

	
